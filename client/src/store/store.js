import { applyMiddleware, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension';
import { userMiddleware } from './middleware/userMiddleware';
import { rootReducer } from './reducers';

export const store = createStore(
    rootReducer,
    composeWithDevTools(
        applyMiddleware ( userMiddleware )
    )
);