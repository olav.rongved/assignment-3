export const ACTION_USER_FETCH = 'user:FETCH'
export const ACTION_USER_SET = 'user:SET'
export const ACTION_USER_ADD = 'user:ADD'
export const ACTION_USER_ERROR = 'user:ERROR'


export const userFetchAction = () => ({
    type: ACTION_USER_FETCH
})


export const userSetAction = payload => ({
    type: ACTION_USER_SET,
    payload
})


export const userErrorAction = payload => ({
    type: ACTION_USER_ERROR,
    payload
})

export const userAddAction = payload => ({
    type: ACTION_USER_ADD,
    payload
})