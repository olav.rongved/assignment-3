import {
    ACTION_USER_FETCH,
    ACTION_USER_SET,
    ACTION_USER_ERROR
} from '../actions/userActions'

// init state
const initialState = {
    fetching: false,
    currentUser: null,
    error: null
}

export function userReducer(state = initialState,action) {
    switch (action.type) {
        case ACTION_USER_FETCH:
            return {
                ...state,
                error: null,
                fetching: true
            }
        
        case ACTION_USER_SET:
            return {
                error: null,
                fetching: false, 
                currentUser: action.payload
            }

        case ACTION_USER_ERROR:
            return {
                ...state,
                fetching: false, 
                error: action.payload
            }
        
        default:
            return state
    }
}
