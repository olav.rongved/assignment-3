import { ACTION_USER_FETCH } from '../actions/userActions'
import { fetchUser } from '../../api/user' // todo:
import { userSetAction, userErrorAction } from '../actions/userActions'

export const userMiddleware = ({ dispatch }) => next => async action => {
    next( action )

    if (action.type === ACTION_USER_FETCH) {
        try {
            const user = await fetchUser()
            dispatch( userSetAction(user))
        }
        catch (err) {
            dispatch(userErrorAction(err.message))
        }
    }
}