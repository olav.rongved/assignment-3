const BASE_URL = 'http://localhost:5000'


export async function fetchUser() {
    const response = await fetch(`${BASE_URL}/users`)
    const json = await response.json()
    return json
}

export async function findUserByName(name) {
    const response = await fetch(`${BASE_URL}/users?name=${name}`)
    const json = await response.json()
    return json
}

export async function validateUserLogin(name) {

    // Find user by name
    // Case 1: User found - Success! redirect - set global state of currentUser
    // Case 2: No user found - create user
    try {
        const userData = await findUserByName(name)
        if (userData.length === 0) {
            console.log("No user found");
            
            console.log("Creating users...");
            console.log("User created");
            const userObject = await createUser(name)
            return userObject

            
        } else {
            return userData[0]
        }
    } catch (e) {
        return e.message;
    }
}

export async function createUser(name) {

    const userData = {
        name: name,
        translations: []
    }

    const config = {
        method: 'POST',
        body: JSON.stringify(userData),
        headers: {
            'Content-Type': 'application/json'
          }
    }

    const response = await fetch(`${BASE_URL}/users`, config)
    const json = await response.json()
    return json
}

// Update user 

export async function updateUser(userID,userData) {

    const config = {
        method: 'PATCH',
        body: JSON.stringify(userData),
        headers: {
            'Content-Type': 'application/json'
          }
    }

    const response = await fetch(`${BASE_URL}/users/${userID}`, config)
    const json = await response.json()
    return json
}