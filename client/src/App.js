
import './style/index.css'

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Login from './pages/Login'
import Profile from './pages/Profile'
import Translations from './pages/Translations'

function App() {
  return (

    <div className="app columns is-centered">
      <div className="column is-half">
        <h1 className="title is-1">ASL translation</h1>
        <p className="subtitle is-3">👆👉👍</p>
        <Router>
          <Switch>
            <Route exact path="/">
              <Login />
            </Route>
            <Route exact path="/translations">
              <Translations />
            </Route>
            <Route path="/profile">
              <Profile />
            </Route>
          </Switch>
        </Router>
      </div>
    </div>
  );
}

export default App;
