import TranslationItemList from '../components/TranslationItemList'
import { useSelector, useDispatch } from 'react-redux'
import { useState } from 'react'
import { updateUser } from '../api/user'
import { userSetAction } from '../store/actions/userActions'
import { useHistory } from 'react-router-dom'

function Translations() {

  const [translation, setTranslation] = useState('')
  const dispatch = useDispatch()
  const { currentUser } = useSelector(state => state.userReducer)
  console.log(currentUser);
  const history = useHistory();



  const submitText = async () => {
    const newTranslation = {
      text: translation,
      deleted: false
    }


    try {
      const newUser = await updateUser(currentUser.id, {
        translations: [...currentUser.translations, newTranslation]
      })
      dispatch(userSetAction(newUser))
      setTranslation("")
    } catch (e) {
      return e.message
    }
    // refresh user

  }

  const redirectToProfile = () => {
    history.push('/profile')
    }


  const onTranslationChange = event => {
    setTranslation(event.target.value)
  }


  // TODO:
  // Get text when button is clicked
  // Push to database as string and reset input field

  // TODO - visualization translation
  // Get string from database or other place
  // All chars to lowercase
  // Split string into array of single characters
  // Use array to get images from assets

  return (
    <div>
      Logged in as {currentUser.name}
      <div className="field">
        <p className="control">
          <input className="textarea" type="textfield" placeholder="Enter text to translate" value={translation} onChange={onTranslationChange} />
        </p>
        <p className="control">
          <button className="button is-info" onClick={submitText}>Translate</button>
          <button className="button is-info" onClick={redirectToProfile}>Profile</button>
        </p>
      </div>
      <TranslationItemList translations={currentUser.translations} />

    </div>
  );

}

export default Translations;