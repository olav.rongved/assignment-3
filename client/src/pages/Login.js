
import { useState } from 'react'
import { useHistory } from "react-router-dom";
import { fetchUser } from '../api/user'

import { useDispatch } from 'react-redux';
import { userSetAction } from '../store/actions/userActions'
import { createUser, findUserByName, validateUserLogin } from '../api/user'

function Login() {

    const [name, setName] = useState('')
    const dispatch = useDispatch()
    const history = useHistory();

    const onChangeLoginForm = event => {
        setName(
            event.target.value)
    }

    


    const handleSubmit = async () => {
        console.log(name);

        try {
            // Case 1 = if username does not exist, then create user 
            // Case 2 = if username does exist, then load user and go to translations page
            const userFound = await validateUserLogin(name)
            dispatch(userSetAction(userFound))
            history.push("/translations");

        } catch (err) {
            console.log(err.message);
        }

    };

    return (
        <>
            <div className="field is-grouped">
                <p className="control is-expanded">
                    <input className="input" type="text" placeholder="Enter your name" onChange={onChangeLoginForm} />
                </p>
                <p className="control">
                    <button className="button is-info" onClick={handleSubmit}>Login</button>
                </p>
            </div>
            {name}
        </>

    )
}

export default Login;