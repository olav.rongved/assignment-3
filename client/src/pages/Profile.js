import { useEffect } from 'react'
import { useLocation } from 'react-router-dom'
import TranslationItemList from '../components/TranslationItemList'
import { useSelector, useDispatch } from 'react-redux'
import { useState } from 'react'
import { updateUser } from '../api/user'
import { userSetAction } from '../store/actions/userActions'

function Profile(props) {


    const [translation, setTranslation] = useState('')
    const dispatch = useDispatch()
    const { currentUser } = useSelector(state => state.userReducer)

    return (
        <div>
            Logged in as {currentUser.name}
            <div className="field">
                <p className="control">
                </p>
                <p className="control">
                    <button className="button is-info">Clear Translations</button>
                </p>
            </div>
            <TranslationItemList translations={currentUser.translations.reverse()} />

        </div>
    )

}

export default Profile;
