import TranslationItem from '../components/TranslationItem'

function TranslationItemList({ translations }) {

    return (
        <>  
            <div className="notification is-light">
                Hello from TranslationItemList
                <ul>
                    {translations.map(translation => <TranslationItem text={translation.text}/>)}
                </ul>
            </div>
        </>
    )

}

export default TranslationItemList;