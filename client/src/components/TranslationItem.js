

import { processTranslationString } from '../utils/asl'
import letters from '../utils/letters'
function TranslationItem({ text }) {

    const processedTranslation = processTranslationString(text)

    return (
        <>
            <li>
                <div className="notification is-info">
                {processedTranslation.map(char => <img className="image-small" src={letters[char]} />)}
                
                </div>
            </li>
        </>
    )

}

export default TranslationItem;

