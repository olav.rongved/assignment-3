//
import Store from '../store/index.js'

function LoginButton() {

    function onChangeLoginForm() {
        console.log("Test");
    }

    return (
        <>
            <div>
                <p>
                    Clicked: <span id="value">0</span> times
                    <button id="increment">+</button>
                    <button id="decrement">-</button>
                    <button id="incrementIfOdd">Increment if odd</button>
                    <button id="incrementAsync">Increment async</button>
                </p>
            </div>
            <Store />
        </>
    )

}

export default LoginButton;