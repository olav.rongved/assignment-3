
import { useState } from 'react'
import { useDispatch } from 'react-redux';
import {userSetAction} from '../store/actions/userActions'
import {validateUserLogin} from '../api/user'

function Login() {

    const [ name, setName ] = useState('')
    const dispatch = useDispatch()

    const onChangeLoginForm = event => {
        setName(
            event.target.value)
            }

    const handleSubmit = async () => {
        console.log(name);

        try {
            // Case 1 = if username does not exist, then create user 
            // Case 2 = if username does exist, then load user and go to translations page
            const userFound = await  validateUserLogin(name)
            dispatch(userSetAction(userFound))

        } catch (err) {
            console.log(err.message);
        }
        
        
        
      };

    return (
        <>
                Name:
                <input type="text" name="name" onChange={onChangeLoginForm}/>
                <button type="button" onClick={handleSubmit}>Log in</button>
                {name}
        </>
        
    )
}

export default Login;