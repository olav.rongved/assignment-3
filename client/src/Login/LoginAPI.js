// API
function fetchUser() {
    user = fetch(users => {
        return users
    }).then(users => {
        return users.json()
    }).catch(err => {
        console.log(err.message);
    })
}

function postUser(username) {
    fetch('localhost:5000/', {
        method: 'POST',
        body: username
    })
    .then(response => response.json())
    .then(result => {
        console.log('Success:', result);
    })
    .catch(err => {
        console.error('Err:', err.message);
    })
}