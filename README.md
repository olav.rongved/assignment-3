# Assignment 3

React app to translate english -> american sign language

### Extra Functionality

- [x] react-redux - (Redux toolkit recommended)
- [ ] Auth0 authenticetion client to manage login and session

## Assignment requirements

- [x] React framework
- [x] React Router to navigate between pages
- [ ] Store the translations in a json database using Node.js. You can make use of the json-server
node library or the typidcode lowdb.
- [ ] Make use of the ContextAPI 

### Functionality

**Startup page**


- [ ] App should start on a "Start Screen"'
- [ ] A user should first see a login screen
- [ ] User needs to be registered in a database solution (lowDB or json-server)
- [ ] Session management to log in user, and redirect to translation page if logged in

**Translation page**
Translates words and sentences to sign language through images
- [ ] User should have an empty input field to enter words and sentences
- [ ] Once user has written desired input, user must click button to translate input
- [ ] Display signlanguage translation
- [ ] Translations must be stored in a node.js database solution
- [ ] (Not required) May limit input to 40 letters

**Profile screen**
- [ ] Display the last 10 translations for current user
- [ ] Translations must be stored in a node.js database solution
- [ ] Button to clear translations - should mark translations as deleted in db and no longer display on profile page
- [ ] Logout button to clear all storage and return to start page
